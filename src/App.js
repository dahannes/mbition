import data from './data.json'

import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import uuidv1 from 'uuid/v1';

import Timeline from 'react-calendar-timeline/lib'
import moment from 'moment'

const items = []

const groups = Object.keys(data.applications).map((key, i) => {

  data.applications[key].sessions.map((session, i) => {
    const getDrivingState = () => {
        switch (session.drivingState) {
          case '0': return 'parking'
          case '0-80': return 'driving'
          case '80+': return 'driving fast'
        }
    }

      let drivingState = getDrivingState()


    items.push({
      id: uuidv1(),
      group: i,
      title: session.device + ' | ' + drivingState,
      start_time: Date.now() + session.start_time,
      end_time: Date.now() + session.end_time
    })
  })

  return {
    id: i,
    title: key
  }
})



class App extends Component {
  render() {
    return (
      <div className="App">
      <Timeline groups={groups}
            items={items}
            defaultTimeStart={moment().add(-100, 'seconds')}
            defaultTimeEnd={moment().add(100, 'seconds')}
            />
      </div>
    );
  }
}

export default App;
